Instalacion y ejecucion del proyecto
1.- Clonamos nuestro repositorio
2.- Nos pararemos sobre la ruta de clonacion y ejecutamos el comando "entorno\Scripts\activate"
3.- Nos movemos a la carpeta "barberia" con "cd barberia"
4.- Ejecutamos el comando python manage.py runserver para que se levante el servidor, si todo esta bien configurado
debera correr la pagina en la ip http://127.0.0.1:8000 o en http://localhost:8000

*se requiere tener instalado la version 3.8.0 de python
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render
from django.db import connection
# Create your views here.

def index(request):
    parrafos = {
        'fotos': [
            {'ruta': 'http://palletsguzman.cl/wp-content/uploads/2019/12/logo-alt.png'},
            {'ruta': 'http://palletsguzman.cl/wp-content/uploads/2019/12/kevin.jpg'},
            {'ruta': 'http://palletsguzman.cl/wp-content/uploads/2019/12/tomas.jpg'},
            {'ruta': 'http://palletsguzman.cl/wp-content/uploads/2019/12/grid0.jpg'},
            {'ruta': 'http://palletsguzman.cl/wp-content/uploads/2019/12/parallax-about.jpg'}
        ],
        'galeria': [
            {'ruta': 'https://2.bp.blogspot.com/-pf4fvI3PXcM/XOKVlo3JrfI/AAAAAAAA9cA/lm55GHviuuQOPt_OqqF-FE_QcOa-uXfhwCLcBGAs/s1600/barbero.jpg'},
            {'ruta': 'https://www.beautymarket.es/peluqueria/fotos/13996_notbmp2.jpg'},
            {'ruta': 'https://image.freepik.com/foto-gratis/hombre-elegante-sentado-barberia_1157-20487.jpg'},
            {'ruta': 'http://nattivos.com/wp-content/uploads/2017/09/Barberia.jpg'},
            {'ruta': 'https://www.barberiaalexarroyo.com/wp-content/uploads/2019/02/barberia-santander-alexarroyo.jpg'},
            {'ruta': 'https://i.pinimg.com/originals/e3/11/71/e311711c059fd807b6e9726263f7bdd0.jpg'}
        ],
        'mapa': "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3329.419631606781!2d-70.57390698420244!3d-33.43837220427979!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662ce549d3358cb%3A0x307ebac2d3e54ec!2sPr%C3%ADncipe%20de%20Gales%205921%2C%20La%20Reina%2C%20Regi%C3%B3n%20Metropolitana!5e0!3m2!1ses!2scl!4v1568385866827!5m2!1ses!2scl",
        'servicios': {
            'subtitulo': 'Experiencia',
            'titulo': 'Servicios',
            'descripcion': 'Conoce nuestras especialidades (corte de cabello, perfilado de barba o afeitado al ras) y experimenta el arte de la barbería clásica.',
            'servicios': [
                {'titulo': 'Corte de cabello', 'descripcion': 'Asesoría, corte y lavado de cabello. Incluye peinado con pomada a elección $14.990'},
                {'titulo': 'Perfilado de barba', 'descripcion': 'Asesoría, arreglo de barba, aplicación de toalla caliente, aceite y limpieza facial $12.990'},
                {'titulo': 'Afeitado al ras', 'descripcion': 'Rasurado con toalla caliente, crema de afeitar y navaja más after shave $14.990'},
                {'titulo': 'Servicio completo', 'descripcion': 'Disfruta el corte de cabello más el perfilado de barba a un precio preferencial $24.990'},
                {'titulo': 'Gift Card', 'descripcion': 'Es la solución ideal para ese regalo especial. Canjéala por cualquier servicio nuestro.'},
                {'titulo': 'Empleos', 'descripcion': '¿Te gustaría trabajar en La Barbería? Envía tu CV a: info@labarberia.cl'}
            ]
        },
        'contacto': {
            'telefono': '+56 9 8975 0668',
            'whatsapp': '+56 9 8975 0668',
            'email': 'info@labarberia.cl'
        },
        'nosotros': {
            'subtitulo': 'Compromiso',
            'titulo': 'Nosotros',
            'descripcion': 'En La Barbería nos hacemos responsables del cuidado integral masculino a través de un servicio de excelencia y arraigado en las antiguas tradiciones, en un ambiente único e inspirado en las clásicas barberías inglesas, con una atención de primer nivel más un amplio catálogo de productos con stock permanente. Experimenta, descubre y vive junto a nosotros el arte de la barbería clásica. ¡Te esperamos!'
        },
        'sucursales': {
            'titulo': 'Sucursales',
            'descripcion': 'Asegura y agenda tu cita con tiempo',
            'direcciones': [
                'La Reina: Príncipe de Gales 5921, local 103, La Reina.',
                'Peñalolén: Av. Consistorial 2100, local 212, Peñalolén.',
                'Providencia: Av. Manuel Montt 1221, local 202, Providencia.'
            ]
        },
        'informacion': {
            'subtitulo': 'Conócenos',
            'titulo': 'La Barbería',
            'descripcion': 'Vive la excelencia y tradición de la barbería clásica con una atención enfocada a satisfacer 100% a nuestros clientes.'
        },
        'footer': 'Desde 2016. La Barbería © Todos los Derechos Reservados.'
    }
    return render(request, 'templates/blog/index.html', parrafos)

